import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:tinted_flutter/jobDetails.dart';
import 'package:tinted_flutter/messageList.dart';

class listJobs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                  padding: EdgeInsets.all(20),
                  icon: Icon(
                    Icons.notifications_none,
                    size: 34,
                    color: Colors.black45,
                  ),
                  onPressed: () {
                    // Navigator.of(context).push(
                    //   CupertinoPageRoute(
                    //     fullscreenDialog: true,
                    //     builder: (context) => messageList(),
                    //   ),
                    // );
                  },
                ),
                Spacer(
                  flex: 1,
                ),
                IconButton(
                  padding: EdgeInsets.all(20),
                  icon: Icon(
                    Icons.account_circle,
                    size: 34,
                    color: Colors.black45,
                  ),
                  onPressed: () {
                    // Navigator.of(context).push(
                    //   CupertinoPageRoute(
                    //     fullscreenDialog: true,
                    //     builder: (context) => messageList(),
                    //   ),
                    // );
                  },
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(25.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.blue.shade900.withOpacity(0.1),
                      spreadRadius: 10,
                      blurRadius: 7,
                      offset: Offset(0, 7), // changes position of shadow
                    ),
                  ],
                ),
                height: 50,
                child: TextField(
                  style: TextStyle(
                    fontSize: 15,
                  ),
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search),
                    suffixIcon: Icon(Icons.tune),
                    // border: ,
                    labelText: 'Search for Jobs, Companies..',
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 20.0, bottom: 20.0),
              alignment: Alignment.centerLeft,
              child: Text("Nearby Jobs",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  )),
            ),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.all(5.0),
                itemCount: 5,
                itemBuilder: (context, index) {
                  return ListTile(
                    // tileColor: Colors.green,
                    title: listItem(context, index),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => jobDetails(index)),
                      );
                    }, // Handle your onTap here.
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

Widget listItem(BuildContext context, int index) {
  return Column(
    children: <Widget>[
      Container(
        child: Hero(
          tag: "MyHero$index",
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.asset(
              "images/office1.png",
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      Column(
        children: [
          Row(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(8.0),
                // color: Colors.black12,
                child: Container(
                  alignment: Alignment.centerLeft,
                  // color: Colors.black12,
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(20.0),
                    // border: Border.all(color: Colors.black, width: 3.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Posted 1 day ago",
                        style: TextStyle(
                          color: Colors.black26,
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 8.0),
                alignment: Alignment.centerLeft,
                child: Text("Visual Designer!",
                    style: TextStyle(
                      // fontSize: 20,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Spacer(
                flex: 1,
              ),
              Container(
                alignment: Alignment.centerRight,
                child: Text("\$20K",
                    style: TextStyle(
                      // fontSize: 20,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(8.0),
                // color: Colors.black12,
                child: Container(
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(20.0),
                    // border: Border.all(color: Colors.black, width: 3.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Apple Inc",
                        style: TextStyle(
                          fontSize: 10,
                          color: Colors.black26,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(5.0),
                // color: Colors.black12,
                child: Container(
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(20.0),
                    // border: Border.all(color: Colors.black, width: 3.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Google Inc",
                        style: TextStyle(
                          color: Colors.black26,
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                ),
              ),
            ],
          ),
          Divider()
        ],
      ),
    ],
  );
}
