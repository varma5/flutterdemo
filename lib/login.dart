import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'Intro1.dart';

// ignore: camel_case_types
class login extends StatelessWidget {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SafeArea(
              child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Text("Welcome back!",
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                    )),
                SizedBox(
                  height: 50,
                ),
                Column(
                  children: [
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      controller: emailController,
                      decoration: InputDecoration(
                          border: UnderlineInputBorder(), labelText: 'Email'),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: passwordController,
                      obscureText: true,
                      // obscuringCharacter: "*",
                      autocorrect: false,
                      enableSuggestions: false,
                      decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Password'),
                    ),
                  ],
                ),
                Column(
                  children: [
                    SizedBox(height: 20),
                    SizedBox(
                      height: 50,
                      child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.blueAccent),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ))),
                        child: Text(
                          'SIGN IN',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          if (emailController.text == "") {
                            return showAlertDialog(
                                context, "Tinted", "Please enter email");
                          } else if (passwordController.text == "") {
                            return showAlertDialog(
                                context, "Tinted", "Please enter password");
                          } else {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => intro1()),
                            );
                          }
                        },
                      ),
                      width: double.infinity,
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.black,
                        textStyle: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                        ),
                      ),
                      onPressed: () {
                        debugPrint("navigation tap");
                      },
                      child: const Text('Already have an account? Login'),
                    ),
                    SizedBox(height: 20),
                    Text("OR"),
                    SizedBox(height: 20),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      child: TextButton(
                        child: Image(
                            image: new AssetImage("images/btnGoogle.png")),
                        onPressed: () {},
                      ),
                      width: double.infinity,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      child: TextButton(
                        child:
                            Image(image: new AssetImage("images/btnApple.png")),
                        onPressed: () {},
                      ),
                      width: double.infinity,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      child: TextButton(
                        child: Image(
                            image: new AssetImage("images/btnFacebook.png")),
                        onPressed: () {},
                      ),
                      width: double.infinity,
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.black,
                        textStyle: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                        ),
                      ),
                      onPressed: () {
                        debugPrint("navigation tap");
                      },
                      child: const Text('New to Tinted? Create an account'),
                    ),
                  ],
                ),
              ],
            ),
          ))
        ],
      ),
    );
  }
}

showAlertDialog(BuildContext context, String title, String content) {
  // set up the button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () => Navigator.pop(context),
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(content),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
