import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'jobDetails.dart';

class messageList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                "Messages",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              )),
          Expanded(
            child: ListView.builder(
              itemCount: 10,
              itemBuilder: (context, index) {
                return ListTile(
                  // tileColor: Colors.green,
                  title: cellMessage(context, index),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => jobDetails(index)),
                    );
                  }, // Handle your onTap here.
                );
              },
            ),
          )
        ],
      )),
    );
  }
}

Widget setImage(String strURL, int index) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(8.0),
    child: Hero(
      tag: "MyHero$index",
      child: Image.asset(
        strURL,
        fit: BoxFit.cover,
        height: 60,
        width: 60,
      ),
    ),
  );
}

Widget cellMessage(BuildContext context, int index) {
  return Column(children: <Widget>[
    Container(
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: setImage("images/office1.png", index),
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "ABC Design",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "Hello, fsdThanks for connecting with us have a great safs",
                  overflow: TextOverflow.fade,
                  style: TextStyle(fontSize: 15, color: Colors.grey
                      // fontWeight: FontWeight.bold,
                      ),
                ),
              ],
            ),
          ),
          Divider(),
        ],
      ),
    )
  ]);
}
