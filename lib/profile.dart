import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class listObject {
  String title;
  var icon;
  listObject({this.title, this.icon});
}

List listItem = [
  listObject(title: "Edit Profile", icon: Icons.person_rounded),
  listObject(title: "My Tinted Resume", icon: Icons.description_rounded),
  listObject(title: "Account", icon: Icons.settings_rounded),
  listObject(title: "Help Center", icon: Icons.help_rounded),
  listObject(title: "Log out", icon: Icons.logout),
];

class profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Image.asset("images/introImg.png"),
          Container(
              height: 100,
              width: 100,
              transform: Matrix4.translationValues(0.0, -50.0, 0.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.asset(
                  "images/office1.png",
                  fit: BoxFit.cover,
                ),
              )),
          Container(
            child: Text("John Smith",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                )),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text("Senior UI Designer at Google"),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: listItem.length,
              itemBuilder: (context, index) {
                return ListTile(
                  // tileColor: Colors.green,
                  title: cellProfile(context, index),
                  onTap: () {
                    // Navigator.of(context).push(
                    //   MaterialPageRoute(
                    //       builder: (context) => jobDetails(index)),
                    // );
                  }, // Handle your onTap here.
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

Widget cellProfile(BuildContext context, int index) {
  return Column(children: <Widget>[
    Container(
      child: Column(
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Icon(
                  listItem[index].icon,
                  size: 25,
                  color: Colors.black45,
                ),
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      listItem[index].title,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Divider(),
        ],
      ),
    )
  ]);
}
