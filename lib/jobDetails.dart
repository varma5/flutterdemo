import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class jobDetails extends StatelessWidget {
  int keys = 0;
  jobDetails(index) {
    keys = index;
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(children: [
        Container(
          child: Stack(
            children: [
              Hero(
                tag: "MyHero$keys",
                child: Image.asset(
                  "images/office1.png",
                  fit: BoxFit.cover,
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: IconButton(
                    onPressed: () => Navigator.pop(context),
                    icon: Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 100,
          width: 100,
          transform: Matrix4.translationValues(0.0, -50.0, 0.0),
          child: Image.asset(
            "images/introImg.png",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          alignment: Alignment.center,
          height: 50,
          child: Text("Senior Product Designer",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
              )),
        ),
        Container(
          height: 25,
          child: Text(
            "Google Los Angels",
          ),
        ),
        Container(
          height: 40,
          child: Text("\$50K - \$90K",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              )),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.all(5.0),
              // color: Colors.black12,
              child: Container(
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(20.0),
                  // border: Border.all(color: Colors.black, width: 3.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("5-6 Years exp.",
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.black45,
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.all(5.0),
              // color: Colors.black12,
              child: Container(
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(20.0),
                  // border: Border.all(color: Colors.black, width: 3.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Full Time",
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.black45,
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.all(5.0),
              // color: Colors.black12,
              child: Container(
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(20.0),
                  // border: Border.all(color: Colors.black, width: 3.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Mid-Level",
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.black45,
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ),
          ],
        ),
        Container(
          alignment: Alignment.centerLeft,
          height: 50,
          child: Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text("Qualifications",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                )),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. \n\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. \n\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. "),
        ),
        Container(
          alignment: Alignment.centerLeft,
          height: 50,
          child: Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text("About the Job",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                )),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 50),
          child: Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. \n\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. \n\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. \n\n\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. \n\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. \n\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id enim id massa bibendum vehicula\. consectetur sem sit amet tellus eleifend, ac pulvinar ante sagittis. "),
        )
      ]),
    ));
  }
}
