import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'listJobs.dart';
import 'jobStatus.dart';
import 'messageList.dart';
import 'profile.dart';

class dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          // appBar: AppBar(
          //   backgroundColor: Color(0xFF3F5AA6),
          //   title: Text("Title text"),
          // ),
          bottomNavigationBar: menu(),
          body: TabBarView(
            children: [
              listJobs(),
              jobStatus(),
              messageList(),
              profile(),
            ],
          ),
        ),
      ),
    );
  }

  Widget menu() {
    return Container(
      color: Colors.white,
      child: TabBar(
        labelColor: Color(0xFF3F5AA6),
        unselectedLabelColor: Colors.black26,
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorPadding: EdgeInsets.all(5.0),
        indicatorColor: Color(0xFF3F5AA6),
        tabs: [
          Tab(
            text: "JOBS",
            icon: Icon(Icons.local_post_office_outlined),
          ),
          Tab(
            text: "JOB STATUS",
            icon: Icon(Icons.assignment),
          ),
          Tab(
            text: "MESSAGES",
            icon: Icon(Icons.account_balance_wallet),
          ),
          Tab(
            text: "PROFILE",
            icon: Icon(Icons.settings),
          ),
        ],
      ),
    );
  }
}
