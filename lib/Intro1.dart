import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'dashboard.dart';

class intro1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Image(height: 20, image: new AssetImage("images/intro1.png")),
                SizedBox(
                  height: 50,
                ),
                Image(
                  image: new AssetImage("images/introText.png"),
                ),
                Spacer(
                  flex: 1,
                ),
                Container(
                  child: Image(
                    image: new AssetImage("images/introImg.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: SizedBox(
          width: double.infinity,
          height: 60,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.redAccent),
            ),
            child: Text("LET'S GET STARTED!"),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => dashboard()),
              );
            },
          ),
        ));
  }
}
